//
//  ContentView.swift
//  Dicer-SwiftUI
//
//  Created by 王佩豪 on 2024/1/10.
//

import SwiftUI

struct ContentView: View {
    
    //使用State属性包装器，当检测到变化时，重建变量
    @State var leftDiceNumber = 1
    @State var rightDiceNumber = 2
    
    
    
    var body: some View {
        
        ZStack {
            Image("background")
                .resizable()
                .edgesIgnoringSafeArea(.all)
            VStack {
                Image("diceeLogo")
                Spacer()
                HStack {
                    DiceView(n: leftDiceNumber)
                    DiceView(n: rightDiceNumber)
                }
                .padding(.horizontal)
                Spacer()
                Button(action: {
                    self.leftDiceNumber = Int.random(in: 1...6)
                    self.rightDiceNumber = Int.random(in: 1...6)
                }, label: {
                    Text("Roll")
                        .font(.system(size: 40))
                        .fontWeight(.heavy)
                        .foregroundColor(.white)
                        
                })
                .background(Color.red)
                Spacer()
            }
            
        }
        
        

    }
}

#Preview {
    ContentView()
}

struct DiceView: View {
    let n:Int
    var body: some View {
            Image("dice\(n)")
                .resizable()
                .aspectRatio(1, contentMode: .fit)
                .padding()
    }
}
