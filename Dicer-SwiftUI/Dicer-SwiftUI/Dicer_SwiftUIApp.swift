//
//  Dicer_SwiftUIApp.swift
//  Dicer-SwiftUI
//
//  Created by 王佩豪 on 2024/1/10.
//

import SwiftUI

@main
struct Dicer_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
