//
//  ContentView.swift
//  H4X0R News
//
//  Created by Angela Yu on 08/09/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    //观察者模式
    @ObservedObject var networkManager = NetworkManager()
    
    var body: some View {
        NavigationView {
            List(networkManager.posts) { post in
                NavigationLink(destination: DetailView(url: post.url)){
                    HStack {
                        Text(String(post.points))
                        Text(post.title)
                    }
                }
                    
            }
            .navigationBarTitle("H4X0R NEWS")
        }
        //在即将出现的时候请求网络，返回数据
        .onAppear {
            self.networkManager.fetchData()
        }
    }
}



#Preview {
    ContentView()
}
//
//let posts = [
//    Post(id: "1", title: "Hello"),
//    Post(id: "2", title: "Bonjour"),
//    Post(id: "3", title: "Hola")
//]
