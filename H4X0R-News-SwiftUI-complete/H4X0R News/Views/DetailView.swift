//
//  DetailView.swift
//  H4X0R News
//
//  Created by 王佩豪 on 2024/1/23.
//  Copyright © 2024 Angela Yu. All rights reserved.
//

import SwiftUI
import WebKit


struct DetailView: View {
    
    let url:String?
    
    var body: some View {
        WebView(urlString: url)
    }
}

#Preview {
    DetailView(url: "https://google.com")
}


