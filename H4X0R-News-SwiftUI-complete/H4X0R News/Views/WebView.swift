//
//  WebView.swift
//  H4X0R News
//
//  Created by 王佩豪 on 2024/1/23.
//  Copyright © 2024 Angela Yu. All rights reserved.
//

import SwiftUI
import WebKit
import SwiftUI



struct WebView: UIViewRepresentable{
    
    let urlString:String?
    
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    func updateUIView(_ uiView: WKWebView, context: Context) {
        if let safeString = urlString{
            if let url = URL(string: safeString){
                let request = URLRequest(url: url)
                uiView.load(request)
            }
        }
    }
}

#Preview {
    WebView(urlString: "https://google.com")
}
