//
//  ContentView.swift
//  I Am Rich
//
//  Created by 王佩豪 on 2024/1/9.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack {
            Color(.systemCyan)
                .edgesIgnoringSafeArea(.all)
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            VStack {
                Text("I Am Rich!")
                    .font(.system(size: 40))
                    .fontWeight(.bold)
                Image("diamond")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 200,height: 200,alignment: .center)
            }
            
        }
        
    }
}

#Preview {
    ContentView()
}
