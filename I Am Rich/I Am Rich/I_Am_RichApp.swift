//
//  I_Am_RichApp.swift
//  I Am Rich
//
//  Created by 王佩豪 on 2024/1/9.
//

import SwiftUI

@main
struct I_Am_RichApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
