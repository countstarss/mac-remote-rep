//
//  ContentView.swift
//  countstarss cord
//
//  Created by 王佩豪 on 2024/1/9.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        // Zstack是允许我们把东西放在一起的堆栈
        ZStack{
            Color(UIColor(red: 0.05, green: 0.63, blue: 0.69, alpha: 1))
                .edgesIgnoringSafeArea(.all)
            VStack {
                Image("peihao")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 150)
                    .clipShape(Circle())
                    .overlay(
                        Circle().stroke(Color.white,lineWidth:5)
                    )
                    
                Text("countstarss")
                    .font(Font.custom("Pacifico-Regular", size: 40))
                    .bold()
                    .foregroundColor(.white)
                Text("iOS Developer")
                    .foregroundColor(.white)
                    .font(.system(size: 20))
                //Divider是分隔线
                Divider()
                //按住command 然后点击extractedView，让它独立出来，类似于extension
                InfoView(text: "+86 17302261341", imageName: "phone.fill")
                InfoView(text: "countstar@163.com", imageName: "envelope.fill")
            }
        }
    }
}

#Preview {
    ContentView()
}


