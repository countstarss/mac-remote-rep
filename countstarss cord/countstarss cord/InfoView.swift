//
//  InfoView.swift
//  countstarss cord
//
//  Created by 王佩豪 on 2024/1/9.
//

import SwiftUI

struct InfoView: View {
    
    let text:String
    let imageName:String
    
    var body: some View {
        RoundedRectangle(cornerRadius: 25)
            .foregroundColor(.white)
            .frame(width: 350,height: 50)
        //使用HStack可以把所有东西放到水平堆栈
            .overlay(HStack {
                Image(systemName: imageName)
                    .foregroundColor(.green)
                Text(text)
            }).foregroundColor(.black)
        
            .padding(.all)
    }
}

//这里的预览只是在这里看，不会对之前写好的代码产生影响
#Preview {
    InfoView(text: "hello,InfoView", imageName: "phone.fill")
        .previewLayout(.sizeThatFits)
}
