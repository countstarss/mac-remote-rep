//
//  countstarss_cordApp.swift
//  countstarss cord
//
//  Created by 王佩豪 on 2024/1/9.
//

import SwiftUI

@main
struct countstarss_cordApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
