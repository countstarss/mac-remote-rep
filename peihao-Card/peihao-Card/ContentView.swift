//
//  ContentView.swift
//  peihao-Card
//
//  Created by 王佩豪 on 2024/1/9.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        // Zstack是允许我们把东西放在一起的堆栈
        ZStack{
            Color(UIColor(red: 0.42, green: 0.36, blue: 0.91, alpha: 0.85))
                .edgesIgnoringSafeArea(.all)
            VStack {
                Text("Hello, world!")
                    .font(Font.custom("Pacifico-Regular", size: 40))
                    .bold()
                    .foregroundColor(.white)
                    
            }
            .padding()
        }
        
    }
}

#Preview {
    ContentView()
}
