//
//  peihao_CardApp.swift
//  peihao-Card
//
//  Created by 王佩豪 on 2024/1/9.
//

import SwiftUI

@main
struct peihao_CardApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
